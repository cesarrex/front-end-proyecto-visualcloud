import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from './guards/auth-guard.service';

import { LogingoogleComponent } from './components/logingoogle/logingoogle.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { FilmsComponent } from './components/films/films.component';
import { UsersComponent } from './components/users/users.component';
import { CatalogueComponent } from './components/catalogue/catalogue.component';
import { CommentsComponent } from './components/comments/comments.component';
import { ReviewsComponent } from './components/reviews/reviews.component';
import { SeriesComponent } from './components/series/series.component';
import { DetallesUserComponent } from './components/detalles-user/detalles-user.component';
import { DetallesFilmComponent } from './components/detalles-film/detalles-film.component';
import { DetallesSeriesComponent } from './components/detalles-series/detalles-series.component';
import { DetallesCommentsComponent } from './components/detalles-comments/detalles-comments.component';
import { DetallesReviewsComponent } from './components/detalles-reviews/detalles-reviews.component';
import { FollowersComponent } from './components/followers/followers.component';
import { UserFilmsComponent } from './components/user-films/user-films.component';
import { UserFilmViewComponent } from './components/user-film-view/user-film-view.component';

const routes: Routes = [
  { path: 'logingoogle', component: LogingoogleComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'home', component: HomeComponent },
  { path: 'films', component: FilmsComponent },
  { path: 'series', component: SeriesComponent },
  { path: 'users', component: UsersComponent },
  { path: 'users_films', component: UserFilmsComponent },
  { path: 'user_film_view', component: UserFilmViewComponent },
  { path: 'followers', component: FollowersComponent },
  { path: 'catalogue', component: CatalogueComponent },
  { path: 'comments', component: CommentsComponent },
  { path: 'review', component: ReviewsComponent },
  { path: 'detallesUser', component: DetallesUserComponent },
  { path: 'detallesFilm', component: DetallesFilmComponent },
  { path: 'detallesSeries', component: DetallesSeriesComponent },
  { path: 'detallesComments', component: DetallesCommentsComponent },
  { path: 'detallesReviews', component: DetallesReviewsComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
