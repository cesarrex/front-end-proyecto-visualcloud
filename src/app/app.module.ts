//Modules
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AuthGuard } from './guards/auth-guard.service';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import {NgxPaginationModule} from 'ngx-pagination';
import { ImgFallbackModule } from 'ngx-img-fallback';

import { SocialLoginModule, SocialAuthServiceConfig } from 'angularx-social-login';
import { GoogleLoginProvider } from 'angularx-social-login';

import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

//Components
import { AppComponent } from './app.component';
import { FilmsComponent } from './components/films/films.component';
import { CommentsComponent } from './components/comments/comments.component';
import { ReviewsComponent } from './components/reviews/reviews.component';
import { UsersComponent } from './components/users/users.component';
import { PlaylistsComponent } from './components/playlists/playlists.component';
import { ChaptersComponent } from './components/chapters/chapters.component';

import { LogingoogleComponent } from './components/logingoogle/logingoogle.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { CatalogueComponent } from './components/catalogue/catalogue.component';
import { SeriesComponent } from './components/series/series.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DetallesUserComponent } from './components/detalles-user/detalles-user.component';
import { DetallesFilmComponent } from './components/detalles-film/detalles-film.component';
import { DetallesCommentsComponent } from './components/detalles-comments/detalles-comments.component';
import { DetallesSeriesComponent } from './components/detalles-series/detalles-series.component';
import { DetallesReviewsComponent } from './components/detalles-reviews/detalles-reviews.component';
import { FollowersComponent } from './components/followers/followers.component';
import { UserFilmsComponent } from './components/user-films/user-films.component';
import { UserFilmViewComponent } from './components/user-film-view/user-film-view.component';

@NgModule({
  declarations: [
    AppComponent,
    FilmsComponent,
    CommentsComponent,
    ReviewsComponent,
    UsersComponent,
    PlaylistsComponent,
    ChaptersComponent,
    LogingoogleComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    NavbarComponent,
    CatalogueComponent,
    SeriesComponent,
    DetallesUserComponent,
    DetallesFilmComponent,
    DetallesCommentsComponent,
    DetallesSeriesComponent,
    DetallesReviewsComponent,
    FollowersComponent,
    UserFilmsComponent,
    UserFilmViewComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    SocialLoginModule,
    HttpClientModule,
    FormsModule,
    NgbModule,
    NgxPaginationModule,
    ImgFallbackModule,
  ],
  providers: [
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(
              '8315577341-l85qhng012cf5turkc023m94jh5h3ng1.apps.googleusercontent.com'
            )
          }
        ]
      } as SocialAuthServiceConfig,
    },
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
