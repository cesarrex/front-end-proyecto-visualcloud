import { Component, OnInit } from '@angular/core';

import { DataService } from 'src/app/service/data/data.service';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

import { User } from '../../models/user.model';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css']
})
export class CommentsComponent implements OnInit {
  comments:any={};
  public err : any;
  pageActual:number = 1;


  constructor(private dataService:DataService, private sanitizer:DomSanitizer, private router: Router) { }

  ngOnInit(): void {
    this.ObtenerComments();
  }

  ObtenerComments(){
  	this.dataService.getComments().subscribe(res => {
      console.log(res);
      this.comments = res;
      console.log(this.comments);
      },
      error=>{
        this.err=error.message;
        console.log(this.err);
        this.err=error.message;
      });
  }

}
