import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetallesCommentsComponent } from './detalles-comments.component';

describe('DetallesCommentsComponent', () => {
  let component: DetallesCommentsComponent;
  let fixture: ComponentFixture<DetallesCommentsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetallesCommentsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetallesCommentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
