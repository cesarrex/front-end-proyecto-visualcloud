import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetallesFilmComponent } from './detalles-film.component';

describe('DetallesFilmComponent', () => {
  let component: DetallesFilmComponent;
  let fixture: ComponentFixture<DetallesFilmComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetallesFilmComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetallesFilmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
