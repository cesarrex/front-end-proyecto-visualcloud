import { Component, OnInit } from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { User } from '../../models/user.model';
import { Film } from '../../models/film.model';
import { NavigationExtras, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { DataService } from 'src/app/service/data/data.service';
import { FormBuilder, FormGroup, NgForm } from '@angular/forms';

@Component({
  selector: 'app-detalles-film',
  templateUrl: './detalles-film.component.html',
  styleUrls: ['./detalles-film.component.css']
})
export class DetallesFilmComponent implements OnInit {

  value= null;
  public err : any;
  film: any;
  deleteId: any;
  editId: any;
  editForm: FormGroup;
  films: Object;

  constructor(private router: Router,
              private dataService:DataService,
              private modal:NgbModal, 
              private modalService: NgbModal, 
              private fb: FormBuilder) {
                const navigation = this.router.getCurrentNavigation();
                this.value = navigation?.extras?.state;
                this.film = this.value.value; }

  ngOnInit(): void {
    console.log(this.film);

    this.editForm = this.fb.group({
      title: [''],
      sinopsis: [''],
      score: [''],
      chapters: [''],
      duration: [''],
      studio: [''],
      genre_id: [''],
      state_id: [''],
      debut: [''],
      image: ['']
    });
  }

  goBack():void{
    this.router.navigate(['films']);
  }

  openEdit(targetModal, film: Film){
    console.log(film);
    this.editId = film.id;
    this.modalService.open(targetModal, {
      centered: true,
      backdrop: 'static',
      size: 'lg'
    });
    this.editForm.patchValue( {
      title: film.title,
      sinopsis: film.sinopsis,
      score: film.score,
      chapters: film.chapters,
      duration : film.duration,
      studio : film.studio,
      genre_id : film.genre_id,
      state_id : film.state_id,
      debut : film.debut,
      image : film.image
    });
  }

  editarFilm(id, film){
    console.log(id);
    console.log(film);
    //console.log(this.editForm.value);
  	this.dataService.updateFilm(id, film).subscribe(res => {
      console.log(res);
      this.films = res;
      this.modalService.dismissAll();
      this.router.navigate(['films']);
      },
      error=>{
        this.err=error.message;
        console.log(this.err);
        this.err=error.message;
      });
  }

  openDelete(targetModal, film: Film){
    console.log(film);
    this.deleteId = film.id;
    this.modalService.open(targetModal, {
      backdrop: 'static',
      size: 'md'
    });
    console.log(this.deleteId);
  }

  eliminarFilm(id): void{
    console.log(id);
    this.dataService.deleteFilm(id).subscribe(res => {
      console.log("Pelicula eliminada correctamente");
      this.modalService.dismissAll();
      this.router.navigate(['films']);
      },
      error=>{
        this.err=error.message;
        console.log(this.err);
        this.err=error.message;
      });
  }

}
