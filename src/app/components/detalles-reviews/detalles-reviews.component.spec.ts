import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetallesReviewsComponent } from './detalles-reviews.component';

describe('DetallesReviewsComponent', () => {
  let component: DetallesReviewsComponent;
  let fixture: ComponentFixture<DetallesReviewsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetallesReviewsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetallesReviewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
