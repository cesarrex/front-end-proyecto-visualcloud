import { Component, OnInit } from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { User } from '../../models/user.model';
import { NavigationExtras, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { DataService } from 'src/app/service/data/data.service';
import { FormBuilder, FormGroup, NgForm } from '@angular/forms';

@Component({
  selector: 'app-detalles-user',
  templateUrl: './detalles-user.component.html',
  styleUrls: ['./detalles-user.component.css']
})
export class DetallesUserComponent implements OnInit {

  value= null;
  public err : any;
  user: any;
  deleteId: any;
  editId: any;
  editForm: FormGroup;
  users: Object;

  constructor(private router: Router,
              private dataService:DataService,
              private modal:NgbModal, 
              private modalService: NgbModal, 
              private fb: FormBuilder) {
                const navigation = this.router.getCurrentNavigation();
                this.value = navigation?.extras?.state;
                this.user = this.value.value; }

  ngOnInit(): void {
    console.log(this.user);

    this.editForm = this.fb.group({
      nickname: [''],
      name: [''],
      surname: [''],
      email: [''],
      password: [''],
      borndate: ['']
    });
  }

  goBack():void{
    this.router.navigate(['users']);
  }

  openEdit(targetModal, user: User){
    console.log(user);
    this.editId = user.id;
    this.modalService.open(targetModal, {
      centered: true,
      backdrop: 'static',
      size: 'lg'
    });
    this.editForm.patchValue( {
      nickname: user.nickname, 
      name: user.name,
      surname: user.surname,
      email: user.email,
      password: user.password,
      borndate: user.borndate
    });
  }

  EditarUser(id, user){
    console.log(id);
    console.log(user);
    //console.log(this.editForm.value);
  	this.dataService.updateUser(id, user).subscribe(res => {
      console.log(res);
      this.users = res;
      this.modalService.dismissAll();
      this.router.navigate(['users']);
      },
      error=>{
        this.err=error.message;
        console.log(this.err);
        this.err=error.message;
      });
  }

  openDelete(targetModal, user: User){
    console.log(user);
    this.deleteId = user.id;
    this.modalService.open(targetModal, {
      backdrop: 'static',
      size: 'md'
    });
    console.log(this.deleteId);
  }

  EliminarUser(id): void{
    console.log(id);
    this.dataService.deleteUser(id).subscribe(res => {
      console.log("Usuario eliminado correctamente");
      this.modalService.dismissAll();
      this.router.navigate(['users']);
      },
      error=>{
        this.err=error.message;
        console.log(this.err);
        this.err=error.message;
      });
  }
}