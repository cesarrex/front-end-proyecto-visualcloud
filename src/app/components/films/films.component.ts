import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, NgForm, FormBuilder } from '@angular/forms';

import { DataService } from 'src/app/service/data/data.service';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

import { User } from '../../models/user.model';
import { Film } from '../../models/film.model';
import { NavigationExtras, Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-films',
  templateUrl: './films.component.html',
  styleUrls: ['./films.component.css']
})
export class FilmsComponent implements OnInit {
  pageActual:number;
  public err : any;
  closeResult = '';
  editForm: FormGroup;
  roleUser=null;
  deleteId: any;
  editId: any;

  navigationExtras: NavigationExtras = {
    state: {
      value: null
    }
  };

  films:any={};
  pelicula: Film = new Film();
  

  constructor( private dataService:DataService, 
                private router: Router,
                private modalService: NgbModal,
                private fb: FormBuilder ) { }

  ngOnInit(): void {
    
    this.roleUser=localStorage.getItem('role');
    if (this.roleUser!="admin"){
      this.router.navigate(['home']);
    }
    this.obtenerFilms();

    this.editForm = this.fb.group({
      title: [''],
      sinopsis: [''],
      score: [''],
      chapters: [''],
      duration: [''],
      studio: [''],
      genre_id: [''],
      state_id: [''],
      debut: [''],
      image: ['']
    });
  }
  obtenerFilms(){
  	this.dataService.getFilms().subscribe(res => {
      console.log(res);
      this.films = res;
      console.log(this.films);
      },
      error=>{
        this.err=error.message;
        console.log(this.err);
        this.err=error.message;
      });
  }

  openCreate(createContent) {
    this.modalService.open(createContent).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  crearFilm( form: NgForm ) {
    console.log(this.pelicula);
    if ( form.invalid ) {
      Swal.fire({
        title: 'Rellene los campos',
      });
      return;
    }
    console.log(this.pelicula);
    Swal.fire({
      allowOutsideClick: false,
      text: 'Espere por favor...',
    });
    Swal.showLoading();
    this.dataService.createFilm( this.pelicula )
      .subscribe( resp => {
        console.log(resp);
        Swal.close();
        this.ngOnInit();
        this.modalService.dismissAll();
      }, (err) => {
        console.log(err);
        Swal.fire({
          title: 'Error al registrar',
          text: err
        });
      });
  }

  verFilm(film: any): void{
    this.navigationExtras.state.value = film;
    this.router.navigate(['detallesFilm'], this.navigationExtras);
  }

  openEdit(targetModal, film: Film){
    console.log(film);
    this.editId = film.id;
    this.modalService.open(targetModal, {
      centered: true,
      backdrop: 'static',
      size: 'lg'
    });
    this.editForm.patchValue( {
      title: film.title,
      sinopsis: film.sinopsis,
      score: film.score,
      chapters: film.chapters,
      duration : film.duration,
      studio : film.studio,
      genre_id : film.genre_id,
      state_id : film.state_id,
      debut : film.debut,
      image : film.image
    });
  }

  editarFilm(id, film){
    console.log(id);
    console.log(film);
    //console.log(this.editForm.value);
  	this.dataService.updateFilm(id, film).subscribe(res => {
      console.log(res);
      this.films = res;
      this.ngOnInit();
      this.modalService.dismissAll();
      },
      error=>{
        this.err=error.message;
        console.log(this.err);
        this.err=error.message;
      });
  }

  openDelete(targetModal, film: Film){
    console.log(film);
    this.deleteId = film.id;
    this.modalService.open(targetModal, {
      backdrop: 'static',
      size: 'md'
    });
    console.log(this.deleteId);
  }

  eliminarFilm(id): void{
    console.log(id);
    this.dataService.deleteFilm(id).subscribe(res => {
      console.log("Pelicula eliminada correctamente");
      this.ngOnInit();
      this.modalService.dismissAll();
      },
      error=>{
        this.err=error.message;
        console.log(this.err);
        this.err=error.message;
      });
  }
}
