import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from 'src/app/service/data/data.service';

export class FollowUser {
  user_id: string;
  follow_id: string;
  action: string;
}

@Component({
  selector: 'app-followers',
  templateUrl: './followers.component.html',
  styleUrls: ['./followers.component.css']
})
export class FollowersComponent implements OnInit {
  followers:any={};
  follower:any={};
  followUser: FollowUser=new FollowUser();
  iniciado:boolean;
  admin:boolean;
  idUser:string;

  constructor(private dataService:DataService, private router: Router) { }

  ngOnInit(): void {
    this.idUser=localStorage.getItem('id');
    this.getFollowers();
  }

  checkIfFollows(follwr){
    console.log(follwr.pivot.user_id);
    if (follwr.pivot.user_id==this.idUser){
      return true;
    }else{
      return false;
    }
  }

  follow(userToFollow){
    this.followUser.follow_id=userToFollow['id'];
    this.followUser.user_id=localStorage.getItem('id');
    this.followUser.action='follow';
    this.dataService.actionFollow(this.followUser).subscribe(res => {
      this.getFollowing();
    });
  }
  unfollow(userToUnfollow){
    console.log(userToUnfollow.id);
    this.followUser.follow_id=userToUnfollow.id;
    this.followUser.user_id=localStorage.getItem('id');
    this.followUser.action='unfollow';
    this.dataService.actionFollow(this.followUser).subscribe(res => {
      this.getFollowing();
    });
  }

  getFollowers(){
    this.dataService.getFollowers(this.idUser).subscribe(res => {
      this.followers = res;
    });
  }
  getFollowing(){
    this.dataService.getFollowing(this.idUser).subscribe(res => {
      this.followers = res;
    });
  }

}
