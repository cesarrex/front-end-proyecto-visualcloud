import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

import { User } from '../../models/user.model';
import { LoginService } from '../../service/authentication/login.service';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  usuario: User = new User();
  recordarme = false;

  constructor( private auth: LoginService,
               private router: Router ) { }

  ngOnInit() {

    if ( localStorage.getItem('email') ) {
      this.usuario.email = localStorage.getItem('email');
      this.recordarme = true;
    }

  }


  login( form: NgForm ) {

    if (  form.invalid ) { return; }

    Swal.fire({
      allowOutsideClick: false,
      text: 'Espere por favor...',
    });
    Swal.showLoading();

    this.auth.login( this.usuario )
      .subscribe( resp => {

        Swal.close();

        if ( this.recordarme ) {
          localStorage.setItem('email', this.usuario.email);
        }

        if (resp['access_token']!=null){
          this.router.navigateByUrl('/home');
        }

      }, (err) => {

        console.log(err);
        Swal.fire({
          title: 'Error al loguear',
          text: err['error']
          
        });
      });

  }

}
