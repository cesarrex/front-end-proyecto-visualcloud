import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { LoginService } from '../../service/authentication/login.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  iniciado: boolean;
  admin: boolean;
  
  constructor(private auth: LoginService,
    private router: Router) { }

  ngOnInit(): void {
    if(localStorage.getItem('token')){
      this.iniciado=true;
    }else{
      this.iniciado=false;
    }
    if(localStorage.getItem('role')=="admin"){
      this.admin=true;
    }else{
      this.admin=false;
    }
  }

  logout(){
    this.iniciado=false;
    this.auth.logout();
    this.router.navigateByUrl('/login');
  }
}