import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { FormBuilder, FormGroup, NgForm } from '@angular/forms';

import { User } from '../../models/user.model';
import { LoginService } from '../../service/authentication/login.service';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  usuario: User;
  registerForm: FormGroup;
  recordarme = false;

  constructor( private auth: LoginService,
               private router: Router) { }

  ngOnInit() {
    this.usuario = new User();
    this.usuario.role='user';
    this.usuario.profilepic='https://cdn.business2community.com/wp-content/uploads/2017/08/blank-profile-picture-973460_640.png';
  }

  onSubmit( form: NgForm ) {

    if ( form.invalid ) {
      Swal.fire({
        title: 'Rellene los campos',
      });
      return;
    }
    console.log(this.usuario);
    Swal.fire({
      allowOutsideClick: false,
      text: 'Espere por favor...',
    });
    Swal.showLoading();

    this.auth.registro( this.usuario )
      .subscribe( resp => {

        console.log(resp);
        Swal.close();

        if ( this.recordarme ) {
          localStorage.setItem('email', this.usuario.email);
        }

        this.router.navigateByUrl('/home');

      }, (err) => {
        console.log(err);
        Swal.fire({
          title: 'Error al registrar',
          text: err['error']
        });
      });
  }


}
