import { Component, OnInit } from '@angular/core';

import { DataService } from 'src/app/service/data/data.service';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

import { User } from '../../models/user.model';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-reviews',
  templateUrl: './reviews.component.html',
  styleUrls: ['./reviews.component.css']
})
export class ReviewsComponent implements OnInit {

  reviews:any={};
  public err : any;
  pageActual:number = 1;

  constructor(private dataService:DataService, private sanitizer:DomSanitizer, private router: Router) { }

  ngOnInit(): void {
    this.ObtenerReviews();
  }

  ObtenerReviews(){
  	this.dataService.getReviews().subscribe(res => {
      console.log(res);
      this.reviews = res;
      console.log(this.reviews);
      },
      error=>{
        this.err=error.message;
        console.log(this.err);
        this.err=error.message;
      });
  }
}
