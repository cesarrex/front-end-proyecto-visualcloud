import { Component, OnInit } from '@angular/core';

import { DataService } from 'src/app/service/data/data.service';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

import { User } from '../../models/user.model';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-series',
  templateUrl: './series.component.html',
  styleUrls: ['./series.component.css']
})
export class SeriesComponent implements OnInit {

  series:any={};
  public err : any;
  pageActual:number = 1;

  constructor(private dataService:DataService, private sanitizer:DomSanitizer, private router: Router) { }

  ngOnInit(): void {
    this.ObtenerSeries();
  }

  ObtenerSeries(){
  	this.dataService.getFilms().subscribe(res => {
      console.log(res);
      this.series = res;
      console.log(this.series);
      },
      error=>{
        this.err=error.message;
        console.log(this.err);
        this.err=error.message;
      });
  }

}
