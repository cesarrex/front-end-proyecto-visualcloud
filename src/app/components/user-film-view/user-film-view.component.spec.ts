import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserFilmViewComponent } from './user-film-view.component';

describe('UserFilmViewComponent', () => {
  let component: UserFilmViewComponent;
  let fixture: ComponentFixture<UserFilmViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserFilmViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserFilmViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
