import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { DataService } from 'src/app/service/data/data.service';

import { Comment } from '../../models/comment.model';
import { Review } from 'src/app/models/review.model';

import Swal from 'sweetalert2';
import { FormBuilder, FormGroup, NgForm } from '@angular/forms';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-user-film-view',
  templateUrl: './user-film-view.component.html',
  styleUrls: ['./user-film-view.component.css']
})
export class UserFilmViewComponent implements OnInit {

  closeResult = '';
  navigationExtras: NavigationExtras = {
    state: {
      value: null
    }
  };

  value= null;
  commentsChapterActive=false;
  film: any;
  comment: Comment = new Comment();
  review: Review = new Review();
  chapters: any={};
  chapterId;
  comments: any={};
  reviews: any={};
  idFilm= null;

  editForm: FormGroup;

  currentUserId;

  constructor(private router: Router, private dataService: DataService, private modalService: NgbModal,
    private fb: FormBuilder) {
    const navigation = this.router.getCurrentNavigation();
    this.value = navigation?.extras?.state;
    this.film = this.value.value;
    this.idFilm = this.value.value['id'];
    this.getChaptersData(this.idFilm);
    this.getReviewsData(this.idFilm);
    this.currentUserId=localStorage.getItem('id');
   }

  ngOnInit(): void {
    console.log(this.film);

    this.editForm = this.fb.group({
      content: ['']
    });
  }

  isUserComment(chapter: any){
    if(chapter.user_id==this.currentUserId){
      return true;
    }else{
      false
    }
  }
  isUserReview(review: any){
    if(review.user_id==this.currentUserId){
      return true;
    }else{
      false
    }
  }

  getChaptersData(idFilm){
    this.dataService.getChaptersOfFilm(idFilm).subscribe(res => {
      this.chapters = res;
      console.log(this.chapters);
    });
  }
  getReviewsData(idFilm){
    this.dataService.getReviewsOfFilm(idFilm).subscribe(res => {
      this.reviews = res;
      console.log(this.reviews);
    });
  }

  seeChapterComments(chapter: any){
    this.comments=null;
    if(this.commentsChapterActive){
      this.commentsChapterActive=false;
    }else if(!this.commentsChapterActive){
      this.commentsChapterActive=true;
      this.chapterId=chapter.id;
    }
    this.dataService.getCommentsOfChapter(this.chapterId).subscribe(res => {
      this.comments = res;
      console.log(this.comments);
    });
  }

  openEditComment(targetModal, comment: Comment){
    console.log(comment);
    this.modalService.open(targetModal, {
      centered: true,
      backdrop: 'static',
      size: 'lg'
    });
    this.editForm.patchValue( {
      content: comment.content
    });
  }

  openCreateReview(createContent) {
    this.modalService.open(createContent, {centered:true}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  openCreateComment(createContent) {
    this.modalService.open(createContent, {centered:true}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  deleteComment(comment: any){
    Swal.fire({
      allowOutsideClick: false,
      text: 'Espere por favor...',
    });
    Swal.showLoading();
    this.dataService.deleteComment(comment.id).subscribe(res => {
      Swal.close();
      console.log(res);
    });
    this.dataService.getCommentsOfChapter(this.chapterId).subscribe(res => {
      this.comments = res;
      console.log(this.comments);
    });
  }

  deleteReview(review: any){
    Swal.fire({
      allowOutsideClick: false,
      text: 'Espere por favor...',
    });
    Swal.showLoading();
    this.dataService.deleteReview(review.id).subscribe(res => {
      Swal.close();
      console.log(res);
    });
    this.getReviewsData(this.idFilm);
  }

  createComment( form: NgForm ) {
    this.comment.user_id=localStorage.getItem('id');
    this.comment.chapter_id=this.chapterId;
    console.log(this.comment);
    if ( form.invalid ) {
      Swal.fire({
        title: 'Rellene los campos',
      });
      return;
    }
    Swal.fire({
      allowOutsideClick: false,
      text: 'Espere por favor...',
    });
    Swal.showLoading();
    this.dataService.createComment( this.comment )
      .subscribe( resp => {
        console.log(resp);
        Swal.close();
        this.ngOnInit();
        this.modalService.dismissAll();
      }, (err) => {
        console.log(err);
        Swal.fire({
          title: 'Error al publicar comentario',
          text: err
        });
      });
      this.dataService.getCommentsOfChapter(this.chapterId).subscribe(res => {
        this.comments = res;
        console.log(this.comments);
      });
  }

  createReview( form: NgForm ) {
    this.review.user_id=localStorage.getItem('id');
    this.review.film_id=this.idFilm;
    console.log(this.review);
    if ( form.invalid ) {
      Swal.fire({
        title: 'Rellene los campos',
      });
      return;
    }
    Swal.fire({
      allowOutsideClick: false,
      text: 'Espere por favor...',
    });
    Swal.showLoading();
    this.dataService.createReview( this.review )
      .subscribe( resp => {
        console.log(resp);
        Swal.close();
        this.ngOnInit();
        this.modalService.dismissAll();
      }, (err) => {
        console.log(err);
        Swal.fire({
          title: 'Error al publicar review',
          text: err
        });
      });
    this.getReviewsData(this.idFilm);
  }

  goBack():void{
    this.router.navigate(['users_films']);
  }

}
