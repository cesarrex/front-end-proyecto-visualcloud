import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { DataService } from 'src/app/service/data/data.service';

@Component({
  selector: 'app-user-films',
  templateUrl: './user-films.component.html',
  styleUrls: ['./user-films.component.css']
})
export class UserFilmsComponent implements OnInit {
  navigationExtras: NavigationExtras = {
    state: {
      value: null
    }
  };
  films:any={};
  iniciado:boolean;
  admin:boolean;
  idUser:string;

  constructor( private dataService:DataService, private router: Router ) { }

  ngOnInit(): void {
    this.getFilmsData();
    this.idUser=localStorage.getItem('id');
  }

  getFilmsData(){
    this.dataService.getFilms().subscribe(res => {
      console.log(res);
      this.films = res;
      console.log(this.films);
    });
  }

  seeFilm(film: any): void{
    this.navigationExtras.state.value = film;
    this.router.navigate(['user_film_view'], this.navigationExtras);
  }

}
