import { Component, OnInit } from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

import { DataService } from 'src/app/service/data/data.service';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

import { User } from '../../models/user.model';
import { NavigationExtras, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { FormBuilder, FormGroup, NgForm } from '@angular/forms';
import { LoginService } from '../../service/authentication/login.service';


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  pageActual:number;

  closeResult = '';
  navigationExtras: NavigationExtras = {
    state: {
      value: null
    }
  };

  users:any={};
  usuario: User = new User();
  recordarme = false;
  public err : any;
  deleteId: any;
  editId: any;
  editForm: FormGroup;
  roleUser=null;
  
  constructor(private dataService:DataService, 
              private sanitizer:DomSanitizer, 
              private router: Router, 
              private modal:NgbModal, 
              private modalService: NgbModal, 
              private auth: LoginService,
              private fb: FormBuilder) { }

  ngOnInit(): void {

    this.roleUser=localStorage.getItem('role');
    if (this.roleUser!="admin"){
      this.router.navigate(['home']);
    }
    this.ObtenerUsers();
    
    this.editForm = this.fb.group({
      nickname: [''],
      name: [''],
      surname: [''],
      email: [''],
      password: [''],
      borndate: ['']
    });
  }

  openCreate(createContent) {
    this.modalService.open(createContent, {centered:true}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  openEdit(targetModal, user: User){
    console.log(user);
    this.editId = user.id;
    this.modalService.open(targetModal, {
      centered: true,
      backdrop: 'static',
      size: 'lg'
    });
    this.editForm.patchValue( {
      nickname: user.nickname, 
      name: user.name,
      surname: user.surname,
      email: user.email,
      password: user.password,
      borndate: user.borndate
    });
  }

  openDelete(targetModal, user: User){
    console.log(user);
    this.deleteId = user.id;
    this.modalService.open(targetModal, {
      backdrop: 'static',
      size: 'md'
    });
    console.log(this.deleteId);
  }

  ObtenerUsers(){
  	this.dataService.getUsers().subscribe(res => {
      console.log(res);
      this.users = res;
      console.log(this.users);
      },
      error=>{
        this.err=error.message;
        console.log(this.err);
        this.err=error.message;
      });
  }

  CrearUser( form: NgForm ) {
    console.log(this.usuario);
    if ( form.invalid ) {
      Swal.fire({
        title: 'Rellene los campos',
      });
      return;
    }
    console.log(this.usuario);
    Swal.fire({
      allowOutsideClick: false,
      text: 'Espere por favor...',
    });
    Swal.showLoading();
    this.dataService.createUser( this.usuario )
      .subscribe( resp => {
        console.log(resp);
        Swal.close();
        this.ngOnInit();
        this.modalService.dismissAll();
      }, (err) => {
        console.log(err);
        Swal.fire({
          title: 'Error al registrar',
          text: err
        });
      });
  }

  VerUser(user: any): void{
    this.navigationExtras.state.value = user;
    this.router.navigate(['detallesUser'], this.navigationExtras);
  }

  EditarUser(id, user){
    console.log(id);
    console.log(user);
    //console.log(this.editForm.value);
  	this.dataService.updateUser(id, user).subscribe(res => {
      console.log(res);
      this.users = res;
      this.ngOnInit();
      this.modalService.dismissAll();
      },
      error=>{
        this.err=error.message;
        console.log(this.err);
        this.err=error.message;
      });
  }

  EliminarUser(id): void{
    console.log(id);
    this.dataService.deleteUser(id).subscribe(res => {
      console.log("Usuario eliminado correctamente");
      this.ngOnInit();
      this.modalService.dismissAll();
      },
      error=>{
        this.err=error.message;
        console.log(this.err);
        this.err=error.message;
      });
  }

}
