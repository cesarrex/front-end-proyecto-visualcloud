import { Injectable } from '@angular/core';

import { CanActivate, Router } from '@angular/router';

import { LoginService } from '../service/authentication/login.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor( private auth: LoginService,
               private router: Router) {}

  canActivate(): boolean  {
    if ( this.auth.estaAutenticado() ) {
      console.log('Estás logueado');
      return true;
    } else {
      this.router.navigateByUrl('/login');
      console.log('No estás logueado');
      return false;
    }
 
  }

}
