export class Comment {
    id: string;
    user_id: string;
    chapter_id: string;
    content: string;
    like: string;
    dislike: string;
    created_at: string;
    updated_at: string;
}
