export class Film {

    id: string; 
    title: string; 
    sinopsis: string; 
    score: string; 
    chapters: string; 
    duration : string; 
    studio : string;  
    genre_id : string;
    state_id : string;
    debut : string; 
    image : string;
}