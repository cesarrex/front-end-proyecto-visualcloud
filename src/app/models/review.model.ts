export class Review {
    id: string;
    user_id: string;
    film_id: string;
    content: string;
    useful_for: string;
    created_at: string;
    updated_at: string;
}
