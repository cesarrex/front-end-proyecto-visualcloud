export class User {

    id: string;
    role: string;
    nickname: string;
    name: string;
    surname: string;
    email: string;
    profilepic: string;
    borndate: string;
    password: string;
    
}