import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../../models/user.model';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private url = 'http://www.visualcloud.com';
  userToken: string;

  headers: HttpHeaders = new HttpHeaders({
    'Content-Type': 'application/json',
    //'Access-Control-Allow-Origin': '*',
    //'X-Requested-With': 'XMLHttpRequest',
    //'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT, DELETE',
    'Accept': 'application/json'
    //'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept',
    //'Access-Control-Allow-Credentials': 'true'
  });

  constructor( private http:HttpClient ) { }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('id');
    localStorage.removeItem('name');
    localStorage.removeItem('role');
  }

  login(usuario: User){
    const authData = {
      ...usuario,
      returnSecureToken: true
    };
    return this.http.post(`${this.url}/api/login`, authData, {headers: this.headers}).pipe(
      map( resp => {
        localStorage.setItem('token', resp['access_token']);
        localStorage.setItem('id', resp['user']['id']);
        localStorage.setItem('name', resp['user']['name']);
        //Get roles
        resp['roles'].map(roles => {
          localStorage.setItem('role', roles['name']);
          console.log(roles['name']);
        });
        
        

        return resp;
      })
    );
  }

  registro( usuario: User ) {

    const authData = {
      ...usuario,
      returnSecureToken: true
    };

    return this.http.post(`${ this.url }/api/register`, authData, {headers: this.headers}).pipe(
      map( resp => {
        this.guardarToken( resp['access_token']);
        return resp;
      })
    );

  }


  private guardarToken( idToken: string ) {
    this.userToken = idToken;
    localStorage.setItem('token', idToken);

    let hoy = new Date();
    hoy.setSeconds( 3600 );

    localStorage.setItem('expira', hoy.getTime().toString() );
  }

  leerToken() {
    if ( localStorage.getItem('token') ) {
      this.userToken = localStorage.getItem('token');
    } else {
      this.userToken = '';
    }

    return this.userToken;
  }


  estaAutenticado(): boolean {
    return !!localStorage.getItem('token');
    /*if ( this.userToken.length < 2 ) {
      return false;
    }

    const expira = Number(localStorage.getItem('expira'));
    const expiraDate = new Date();
    expiraDate.setTime(expira);

    if ( expiraDate > new Date() ) {
      return true;
    } else {
      return false;
    }*/
  }
}