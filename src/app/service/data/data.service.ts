import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private url = 'http://www.visualcloud.com';

  headers: HttpHeaders = new HttpHeaders({
    'Content-Type': 'application/json',
    //'Access-Control-Allow-Origin': '*',
    'Authorization': `Bearer ${localStorage.getItem('token')}`,
    //'X-Requested-With': 'XMLHttpRequest',
    //'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT, DELETE',
    'Accept': 'application/json'
    //'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'
  });

  constructor( private httpClient:HttpClient ) { }

  getFilms(){
    return this.httpClient.get(`${this.url}/api/film/index`, {headers: this.headers})
  }
  showFilm(id){
    return this.httpClient.get(`${this.url}/api/film/show`+ id, {headers: this.headers})
  }
  getChaptersOfFilm(id){
    return this.httpClient.get(`${this.url}/api/film/chapters/`+ id, {headers: this.headers})
  }
  getReviewsOfFilm(id){
    return this.httpClient.get(`${this.url}/api/film/reviews/`+ id, {headers: this.headers})
  }
  createFilm(film){
    return this.httpClient.post(`${this.url}/api/film/create/`, film , {headers: this.headers})
  }
  updateFilm(id, film){
    return this.httpClient.post(`${this.url}/api/film/update/` + id, film, {headers: this.headers})
  }
  deleteFilm(id:number){
    return this.httpClient.delete(`${this.url}/api/film/destroy/` + id, {headers: this.headers})
  }

  getCommentsOfChapter(id){
    return this.httpClient.get(`${this.url}/api/chapter/comments/`+ id, {headers: this.headers})
  }

  getComments(){
    return this.httpClient.get(`${this.url}/api/comment/index`, {headers: this.headers})
  }
  showComment(id){
    return this.httpClient.get(`${this.url}/api/comment/edit`+ id, {headers: this.headers})
  }
  createComment(comment){
    return this.httpClient.post(`${this.url}/api/comment/create`, comment , {headers: this.headers})
  }
  updateComment(id, comment){
    return this.httpClient.post(`${this.url}/api/comment/update/` + id, comment, {headers: this.headers})
  }
  deleteComment(id:number){
    return this.httpClient.delete(`${this.url}/api/comment/destroy/` + id, {headers: this.headers})
  }

  getUsers(){
    return this.httpClient.get(`${this.url}/api/user/index`, {headers: this.headers})
  }
  getFollowers(id){
    return this.httpClient.get(`${this.url}/api/user/followers/`+id, {headers: this.headers})
  }
  getFollowing(id){
    return this.httpClient.get(`${this.url}/api/user/following/`+id, {headers: this.headers})
  }
  showUser(id){
    return this.httpClient.get(`${this.url}/api/user/show/`+ id, {headers: this.headers})
  }
  createUser(user){
    return this.httpClient.post(`${this.url}/api/user/create`, user , {headers: this.headers})
  }
  updateUser(id, user){
    return this.httpClient.post(`${this.url}/api/user/update/` + id, user, {headers: this.headers})
  }
  deleteUser(id:number){
    return this.httpClient.delete(`${this.url}/api/user/destroy/` + id, {headers: this.headers})
  }
  actionFollow(data){
    return this.httpClient.post(`${this.url}/api/user/follow`, data, {headers: this.headers})
  }

  getReviews(){
    return this.httpClient.get(`${this.url}/api/review/index`, {headers: this.headers})
  }
  createReview(review){
    return this.httpClient.post(`${this.url}/api/review/create`, review , {headers: this.headers})
  }
  deleteReview(id:number){
    return this.httpClient.delete(`${this.url}/api/review/destroy/` + id, {headers: this.headers})
  }
}